package in.app.controller;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import in.app.Configuration.HibernateUtil;


public class InsertUserDetailsUsingStoredProcedure 
{

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		public int InsertUserDetailsUsingStoredProcedure(String userName, String PassWord, String firstName, String lastName,String email, String phoneNum, int isActive) 
		{		  
			 int rowsAffected = 0;
			 StoredProcedureQuery count = session.createStoredProcedureQuery("addUserDetails_SHA256");
		    // count.registerStoredProcedureParameter("in_userid", Integer.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_username", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_password", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_firstname", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_lastname", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_email", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_phone", String.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("in_isactive", Integer.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("out_noofrowsaffected", Integer.class, ParameterMode.OUT);
		    
		     count.setParameter("in_username", userName);
		     count.setParameter("in_password", PassWord);
		     count.setParameter("in_firstname", firstName);
		     count.setParameter("in_lastname", lastName);
		     count.setParameter("in_email", email);
		     count.setParameter("in_phone", phoneNum);
		     count.setParameter("in_isactive", isActive);
		     count.execute();
		     rowsAffected =  (Integer) count.getOutputParameterValue("out_noofrowsaffected");
		        
		     System.out.println("No of Rows Affected :"+rowsAffected);
		     session.close();
		     return rowsAffected;
	         
	         
		}
		 
		 
}
