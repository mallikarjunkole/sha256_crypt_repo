package in.app.controller;

import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import in.app.Configuration.HibernateUtil;
import in.app.entity.UserRegistration;


public class getUserDetailsByUserID 
{
	 SessionFactory sf = HibernateUtil.getSessionFactory();
	 Session session = sf.openSession();
	 public String getUserDetailsByUserID( int in_User_ID) 
	 {		 
		 StoredProcedureQuery count = session.createStoredProcedureQuery("getUserDetails");
	     count.registerStoredProcedureParameter("in_userid", Integer.class, ParameterMode.IN);
	     count.registerStoredProcedureParameter("out_username", String.class, ParameterMode.OUT);
	     count.registerStoredProcedureParameter("out_password", String.class, ParameterMode.OUT);
	     count.registerStoredProcedureParameter("out_email", String.class, ParameterMode.OUT);
	 
	     count.setParameter("in_userid", in_User_ID);
	     count.execute();
	 
	     String userName =  (String) count.getOutputParameterValue("out_username");
	     String Password =  (String) count.getOutputParameterValue("out_password");
	     String Email =  (String) count.getOutputParameterValue("out_email");
	     if(userName != null )
	     {
	     
	    	 System.out.println("UserName:"+userName+" Password:"+Password+" Email:"+Email );
	     }else
	     {
	    	System.out.println("Entry is Not available for User ID :"+in_User_ID+"."); 
	     }
	     session.close();
		return Email;
      }
	 

		public void getDecryptedPSWDByUserID(int in_userid)
		{
			 StoredProcedureQuery count = session.createStoredProcedureQuery("getDecrypted_pswd");
		     count.registerStoredProcedureParameter("in_userid", Integer.class, ParameterMode.IN);
		     count.registerStoredProcedureParameter("decrtpt_pswd", String.class, ParameterMode.OUT);
		     count.setParameter("in_userid", in_userid);
		     count.execute();
		 
		     String decryptedPSWD =  (String) count.getOutputParameterValue("decrtpt_pswd");
		    
		     if(decryptedPSWD != null)
		     {
		    	 System.out.println("Decrypted password For User ID " +in_userid+ " is :: "+decryptedPSWD );
		     }else
		     {
		    	 System.out.println("Users ID " +in_userid+ " is not available in DB." );
		     }
		     session.close();
		}

		

	
}
